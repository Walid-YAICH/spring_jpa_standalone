/*
 * Copyright 2018 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import tn.esprit.conf.AppConf;

/**
 * @author Walid YAICH
 */
public class Main {

	private static Logger logger = LoggerFactory.getLogger(Main.class);
	
	public static void main(String[] args) {
		ApplicationContext ctx = new AnnotationConfigApplicationContext(AppConf.class);
//		IFicheStageRepository ficheStageService = ctx.getBean("ficheETEService", IFicheStageRepository.class);
//		
//		String titre = " Mise en place d'une gestion automatisée des enviroennements de développements ";
//		String descriptionSujetStage = "Dans le département informatique en charge des matières premières, "
//				+ "je participerai à la mise en place d'un nouveau système de gestion es environnements "
//				+ "de développements à la volée qui permettra aux équipes IT de créer à la volée "
//				+ "un environneents complet de tests (infrastructure + applicatif), qui pourra être "
//				+ "detruit une fois les tests terminés. * Approprier les règles de fonctionnement "
//				+ "de l'environnement de développement. * Coder / paramétrer et documenter "
//				+ "ce qui est produit en garantissant l'intégration des plateformes dans "
//				+ "le systeme d'information SG dans le respect des standards et des procédures "
//				+ "du référentiel méthodologique SG. *Respecter les normes et procédures en vigueur "
//				+ "(architecture, sécurité, qualité, documentation). "
//				+ "*Effectuer un reporting de l'activité sur le composant produit. "
//				+ "*Définisser les cas de test unitaires sur le composant produit. "
//				+ "*Assurer de la conformité qux spécifications techniques. "
//				+ "Dans le cas échéant, réaliser des tests de performance. "
//				+ "*Respecter les normes de qualité définies au sein du projet, "
//				+ "suivez de l'avancement des développements, le reporting sur les status. "
//				+ "Dans le cas échéant, fournisser des éléments d'informations et d'analyse à "
//				+ "des interlocuteurs internes à l'équipe liées à la production. ";
//		
//		ficheStageService.addFiche(new FicheStage(titre, descriptionSujetStage, LocalDate.now(), LocalDate.now(), TypeStage.QUATRIEME));
		

	}
	
	
}

