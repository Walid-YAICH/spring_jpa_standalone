package tn.esprit.conf;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(DBConf.class)
@ComponentScan("tn.esprit.repository")
public class AppConf {

}
