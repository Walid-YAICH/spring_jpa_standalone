package tn.esprit.repository;

import javax.annotation.PreDestroy;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import tn.esprit.entity.FicheStage;

@Service("ficheETEService")
@Transactional
public class FicheStageETERepository implements IFicheStageRepository {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@PersistenceContext
	EntityManager entityManager;
	
	@Override
	public Long addFiche(FicheStage ficheStage) {
		entityManager.persist(ficheStage);
		return ficheStage.getId();
	}
	
	
	public FicheStage getFicheById(Long id) {
		return entityManager.find(FicheStage.class, id);
	}

	@PreDestroy
	public void preDestroy() {
		logger.trace("Juste avant la destruction de l'ApplicationContext");
	}
	
}
