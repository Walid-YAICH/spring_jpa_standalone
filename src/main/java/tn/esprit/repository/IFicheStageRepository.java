package tn.esprit.repository;

import tn.esprit.entity.FicheStage;

public interface IFicheStageRepository {

	public Long addFiche(FicheStage ficheStage);
	public FicheStage getFicheById(Long id);
}