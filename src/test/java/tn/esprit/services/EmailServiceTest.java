package tn.esprit.services;


import javax.mail.MessagingException;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * https://junit.org/junit5/docs/current/user-guide/#writing-tests-test-execution-order
 * https://howtodoinjava.com/junit5/junit-5-maven-dependency-pom-xml-example/
 * @author yaich
 * To run all test of your project : mvn surefire:test
 */
@TestMethodOrder(OrderAnnotation.class)
public class EmailServiceTest {
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	
	@Test
	@Order(1)
	public void sendEmailTestGmail() throws MessagingException {
		logger.trace("order = 1");
		EmailService emailService = new EmailService("testemailsend898@gmail.com", "");
		emailService.sendEmail("walid.yaich@gmail.com", "subject test", "text test");
	}
	
	
	@Order(2)
	@Test
	public void sendEmailTestEsprit() throws MessagingException {
		logger.trace("order = 2");
		EmailService emailService = new EmailService("arctic.pi@esprit.tn", "");
		emailService.sendEmail("walid.yaich@gmail.com", "subject test", "text test");
	}
	
}