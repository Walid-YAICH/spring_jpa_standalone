package tn.esprit.services;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import tn.esprit.conf.AppConf;
import tn.esprit.entity.FicheStage;
import tn.esprit.entity.TypeStage;
import tn.esprit.repository.IFicheStageRepository;

/**
 * surefire:test -Dtest=FicheStageETERepositoryTest
 * https://junit.org/junit5/docs/current/user-guide/#writing-tests-test-execution-order
 * https://howtodoinjava.com/junit5/junit-5-maven-dependency-pom-xml-example/
 * @author yaich
 *
 */
@TestMethodOrder(OrderAnnotation.class)
@SpringJUnitConfig(AppConf.class)
public class FicheStageETERepositoryTest {
	
	private static Logger logger = LoggerFactory.getLogger(FicheStageETERepositoryTest.class);

	@Autowired
	IFicheStageRepository ficheStageETERepository;
	
	@BeforeAll
	public static void init() {
		logger.trace("run once before all tests : FicheStageETERepositoryTest");
	}
	
	@BeforeEach
	public void initEach() {
		logger.trace("run before every test : FicheStageETERepositoryTest");
	}
	
	
	@Order(1)
	@Test
	public void addFiche() {
		assertNotNull(ficheStageETERepository);
		String titre = " Mise en place d'une gestion automatisée des enviroennements de développements ";
		String descriptionSujetStage = "Dans le département informatique en charge des matières premières, "
				+ "je participerai à la mise en place d'un nouveau système de gestion es environnements "
				+ "de développements à la volée qui permettra aux équipes IT de créer à la volée "
				+ "un environneents complet de tests (infrastructure + applicatif), qui pourra être "
				+ "detruit une fois les tests terminés. * Approprier les règles de fonctionnement "
				+ "de l'environnement de développement. * Coder / paramétrer et documenter "
				+ "ce qui est produit en garantissant l'intégration des plateformes dans "
				+ "le systeme d'information SG dans le respect des standards et des procédures "
				+ "du référentiel méthodologique SG. *Respecter les normes et procédures en vigueur "
				+ "(architecture, sécurité, qualité, documentation). "
				+ "*Effectuer un reporting de l'activité sur le composant produit. "
				+ "*Définisser les cas de test unitaires sur le composant produit. "
				+ "*Assurer de la conformité qux spécifications techniques. "
				+ "Dans le cas échéant, réaliser des tests de performance. "
				+ "*Respecter les normes de qualité définies au sein du projet, "
				+ "suivez de l'avancement des développements, le reporting sur les status. "
				+ "Dans le cas échéant, fournisser des éléments d'informations et d'analyse à "
				+ "des interlocuteurs internes à l'équipe liées à la production. ";
		
		Long generatedId = ficheStageETERepository.addFiche(new FicheStage(titre, descriptionSujetStage, 
				LocalDate.now(), LocalDate.now(), TypeStage.QUATRIEME));
		
		assertTrue("Id non généré", generatedId > 0);
	}
	
	@Order(2)
	@Test
	public void getFicheById() {
		assertNotNull(ficheStageETERepository);
		FicheStage ficheStage = ficheStageETERepository.getFicheById(1l);
		assertNotNull(ficheStage, "Fiche non trouvée");
	}
	
	
	@Test
	@DirtiesContext
	void testPredestroy() {
		assertEquals(1, 1);
		logger.trace("l'ApplicationContext va etre détruit a la fin de ce test");
	}
	
}
